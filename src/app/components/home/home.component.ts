import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { profileDetail } from "src/app/interfaces/profile-detail-state.interface";
import { WorkingStatus } from "src/app/interfaces/working-status-state.interface";
import { loadProfileDetail } from "src/app/stores/actions/profile-detail.actions";
import { loadWorkingStatus } from "src/app/stores/actions/working-status.actions";
import { getProfileDetail } from "src/app/stores/selectors/profile-detail.selectors";
import { getWorkingStatus } from "src/app/stores/selectors/working-status.selectors";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent {
  workingStatus$: Observable<WorkingStatus> = new Observable();
  profileDetail$: Observable<profileDetail> = new Observable();
  imageLoading: boolean = true;

  constructor(private store: Store) {
    this.store.dispatch(loadWorkingStatus());
    this.store.dispatch(loadProfileDetail());
    this.workingStatus$ = this.store.select(getWorkingStatus);
    this.profileDetail$ = this.store.select(getProfileDetail);
  }

  onImageLoaded() {
    this.imageLoading = false;
  }
}
