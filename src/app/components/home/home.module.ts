import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { IndexModule } from "src/app/modules";

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, HomeRoutingModule, IndexModule],
})
export class HomeModule {}
