interface developSkill {
  level: number;
  name: string;
}

interface education {
  branch: string;
  name: string;
  time: string;
}

export interface profileDetail {
  image: {
    url: string;
    path: string;
  };
  name: string;
  surname: string;
  expectedSalary: number;
  position: string[];
  developSkill: developSkill[];
  educationList: education[];
  qualificationList: string[];
}

export interface profileDetailState {
  loading: boolean;
  error: string;
  status: boolean;
  profileDetail: profileDetail;
}
