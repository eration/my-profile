export interface WorkingStatus {
  isWorking: boolean;
}

export interface WorkingStatusState {
  loading: boolean;
  status: boolean;
  error: string;
  workingStatus: WorkingStatus;
}
