import { createFeatureSelector, createSelector } from "@ngrx/store";
import { profileDetailState } from "src/app/interfaces/profile-detail-state.interface";

export const profileDetailFeatureSelector =
  createFeatureSelector<profileDetailState>("profileDetailReducer");

export const getProfileDetail = createSelector(
  profileDetailFeatureSelector,
  (state) => state.profileDetail
);

export const getProfileDetailLoading = createSelector(
  profileDetailFeatureSelector,
  (state) => state.loading
);

export const getProfileDetailStatus = createSelector(
  profileDetailFeatureSelector,
  (state) => state.status
);

export const getProfileDetailError = createSelector(
  profileDetailFeatureSelector,
  (state) => state.error
);
