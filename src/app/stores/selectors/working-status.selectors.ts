import { createFeatureSelector, createSelector } from "@ngrx/store";
import { WorkingStatusState } from "src/app/interfaces/working-status-state.interface";

export const workingStatusFeature = createFeatureSelector<WorkingStatusState>(
  "workingStatusReducer"
);

export const getWorkingStatus = createSelector(
  workingStatusFeature,
  (state: WorkingStatusState) => state.workingStatus
);

export const getWorkingStatusLoading = createSelector(
  workingStatusFeature,
  (state: WorkingStatusState) => state.loading
);

export const getWorkingStatusError = createSelector(
  workingStatusFeature,
  (state: WorkingStatusState) => state.error
);

export const getWorkingStatusStatus = createSelector(
  workingStatusFeature,
  (state: WorkingStatusState) => state.status
);
