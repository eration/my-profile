import { createReducer, on } from "@ngrx/store";
import { WorkingStatusState } from "src/app/interfaces/working-status-state.interface";
import {
  loadWorkingStatus,
  loadWorkingStatusFailure,
  loadWorkingStatusSuccess,
} from "../actions/working-status.actions";

export const initState: WorkingStatusState = {
  loading: false,
  error: "",
  status: false,
  workingStatus: {
    isWorking: false,
  },
};

export const workingStatusReducer = createReducer(
  initState,
  on(loadWorkingStatus, (state) => ({
    ...state,
    loading: true,
  })),
  on(loadWorkingStatusSuccess, (state, { status, workingStatus }) => ({
    ...state,
    status,
    workingStatus,
    loading: false,
  })),
  on(loadWorkingStatusFailure, (state, { error }) => ({
    ...state,
    error,
    loading: false,
  }))
);
