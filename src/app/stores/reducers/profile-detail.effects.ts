import { createReducer, on } from "@ngrx/store";
import { profileDetailState } from "src/app/interfaces/profile-detail-state.interface";
import {
  loadProfileDetail,
  loadProfileDetailFailure,
  loadProfileDetailSuccess,
} from "../actions/profile-detail.actions";

export const initState: profileDetailState = {
  loading: false,
  error: "",
  status: false,
  profileDetail: {
    image: {
      url: "",
      path: "",
    },
    expectedSalary: 0,
    name: "",
    surname: "",
    position: [],
    developSkill: [],
    educationList: [],
    qualificationList: [],
  },
};

export const profileDetailReducer = createReducer(
  initState,
  on(loadProfileDetail, (state) => ({
    ...state,
    loading: true,
  })),
  on(loadProfileDetailSuccess, (state, { status, profileDetail }) => ({
    ...state,
    loading: false,
    status,
    profileDetail,
  })),
  on(loadProfileDetailFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  }))
);
