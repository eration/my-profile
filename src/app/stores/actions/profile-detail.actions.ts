import { createAction, props } from "@ngrx/store";
import { profileDetail } from "src/app/interfaces/profile-detail-state.interface";

export const loadProfileDetail = createAction("[profile] load profile");

export const loadProfileDetailSuccess = createAction(
  "[profile] load profile success",
  props<{ status: boolean; profileDetail: profileDetail }>()
);

export const loadProfileDetailFailure = createAction(
  "[profile] load profile failure",
  props<{ error: string }>()
);
