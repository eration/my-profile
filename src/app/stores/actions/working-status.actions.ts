import { createAction, props } from "@ngrx/store";
import { WorkingStatus } from "src/app/interfaces/working-status-state.interface";

export const loadWorkingStatus = createAction(
  "[working-status] load working status"
);

export const loadWorkingStatusSuccess = createAction(
  "[working-status] load working status success",
  props<{ status: boolean; workingStatus: WorkingStatus }>()
);

export const loadWorkingStatusFailure = createAction(
  "[working-status] load working status failure",
  props<{ error: string }>()
);
