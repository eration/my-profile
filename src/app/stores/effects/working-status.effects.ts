import { Actions, createEffect, ofType } from "@ngrx/effects";
import {
  loadWorkingStatus,
  loadWorkingStatusFailure,
  loadWorkingStatusSuccess,
} from "../actions/working-status.actions";
import { catchError, map, mergeMap, of } from "rxjs";
import { FirebaseService } from "src/app/services/firebase.service";
import { Injectable } from "@angular/core";

@Injectable()
export class WorkingStatusEffect {
  constructor(
    private actions$: Actions,
    private firebaseService: FirebaseService
  ) {}

  getWorkingStatus$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadWorkingStatus),
      map((state) => state),
      mergeMap((state) =>
        this.firebaseService.getWorkingStatus().pipe(
          map((workingStatus) =>
            loadWorkingStatusSuccess({
              status: true,
              workingStatus: { isWorking: workingStatus },
            })
          ),
          catchError((e) =>
            of(
              loadWorkingStatusFailure({
                error: "ไม่สามารถดึงข้อมูล working status ได้",
              })
            )
          )
        )
      )
    )
  );
}
