import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { FirebaseService } from "src/app/services/firebase.service";
import {
  loadProfileDetail,
  loadProfileDetailFailure,
  loadProfileDetailSuccess,
} from "../actions/profile-detail.actions";
import { catchError, map, mergeMap, of } from "rxjs";

@Injectable()
export class ProfileDetailEffect {
  constructor(
    private actions$: Actions,
    private firebaseService: FirebaseService
  ) {}

  getProfileDetail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadProfileDetail),
      map((state) => state),
      mergeMap((data) =>
        this.firebaseService.getProfileDetail().pipe(
          map((profileDetail) =>
            loadProfileDetailSuccess({ status: true, profileDetail })
          ),
          catchError((e) =>
            of(
              loadProfileDetailFailure({
                error: "ไม่สามารถโหลดข้อมูลโปรไฟลได้",
              })
            )
          )
        )
      )
    )
  );
}
