import { Injectable } from "@angular/core";
import { Database, object, ref } from "@angular/fire/database";
import { Observable, map, mergeMap } from "rxjs";
import { profileDetail } from "../interfaces/profile-detail-state.interface";

@Injectable({
  providedIn: "root",
})
export class FirebaseService {
  constructor(private database: Database) {}

  public getProfileDetail(): Observable<profileDetail> {
    const dbRef = ref(this.database, "profile-detail");
    return object(dbRef).pipe<profileDetail>(map((c) => c.snapshot.val()));
  }

  public getWorkingStatus(): Observable<boolean> {
    const dbRef = ref(this.database, "workingStatus");
    return object(dbRef).pipe<boolean>(map((c) => c.snapshot.val()));
  }
}
