import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { workingStatusReducer } from "./stores/reducers/working-status.reducers";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { initializeApp, provideFirebaseApp } from "@angular/fire/app";
import { environment } from "../environments/environment";
import { provideDatabase, getDatabase } from "@angular/fire/database";
import { provideStorage, getStorage } from "@angular/fire/storage";
import { WorkingStatusEffect } from "./stores/effects/working-status.effects";
import { profileDetailReducer } from "./stores/reducers/profile-detail.effects";
import { ProfileDetailEffect } from "./stores/effects/profile-detail.effects";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({ workingStatusReducer, profileDetailReducer }),
    EffectsModule.forRoot([WorkingStatusEffect, ProfileDetailEffect]),
    BrowserAnimationsModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideDatabase(() => getDatabase()),
    provideStorage(() => getStorage()),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
